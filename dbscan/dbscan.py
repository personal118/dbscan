import logging
import numpy as np

from utils import get_logger

def default_distance(x,y):
	return np.linalg.norm(x-y)

class DBSCAN:

	def __init__(self) -> None:
		pass

	def __init(self, points, eps, min_count_of_points, dist = default_distance, verbose=False) -> None:
		if verbose:
			self.logger = get_logger("DBSCAN", level=logging.DEBUG)


		self.points = points
		self.count_of_points = len(points)
		self.eps = eps
		self.min_count_of_points = min_count_of_points

		self.noise_cluster_marker = 0
		self.count_of_clusters = 0
		self.clusters = {self.noise_cluster_marker: []}

		self.visited_points = set()
		self.clustered_points = {}

		self.distance_function = dist
		self.distance_matrix = self.__compute_distnace_matrix()

	def fit(self, points, eps, min_count_of_points, dist = default_distance, verbose=False) -> None:
		self.__init(points,eps,min_count_of_points,dist,verbose)

		self.logger.debug(f"eps = {eps}, m = {min_count_of_points}.")
		self.logger.debug(f"Size of X = {len(points)}.")
		self.logger.debug("Fit X start...")
		for point_index in range(0, self.count_of_points):
			if point_index in self.visited_points:
				continue
			self.visited_points.add(point_index)

			neighbours = self.__get_neighbours(point_index)
			if len(neighbours) < self.min_count_of_points:
				self.logger.debug("Found noise point.")
				self.clusters[self.noise_cluster_marker].append(point_index)
				self.clustered_points[point_index] = self.noise_cluster_marker
			else:
				self.logger.debug("Found root point.")
				self.logger.debug("Starting expand cluster...")
				self.__pick_new_cluster(point_index, neighbours)

		self.logger.debug("All pointes was visited.")
		conclusions = []
		for i in range(0, self.count_of_points):
			conclusions.append(self.clustered_points[i])
		self.logger.debug(f"Found {self.count_of_clusters} clusters.")
		if len(self.clusters[self.noise_cluster_marker]) != 0:
			self.logger.debug(f"Found {len(self.clusters[self.noise_cluster_marker])} noise points.")

		self.logger.debug(f"finished")
		return conclusions
		
	def __compute_distnace_matrix(self):
		self.logger.debug("Compute distance matrix...")
		distance_matrix = [None] * self.count_of_points
		for i in range(0, self.count_of_points - 1):
			distance_vector = [None] * self.count_of_points

			for j in range(i+1, self.count_of_points):
				x = self.points[i]
				y = self.points[j]
				distance_vector[j] = self.distance_function(x, y)

			distance_matrix[i] = distance_vector
		return distance_matrix
	
	def __get_distance(self, x_index: int, y_index: int) -> float:
		"""Return distance using indexs of points in points list"""
		# TODO Проверка на выход за границу чтения
		if x_index == y_index:
			return 0 

		if x_index > y_index:
			return self.__get_distance(y_index, x_index)

		if self.distance_matrix[x_index][y_index] == None:
			raise Exception(f'distance not found btw {x_index} and {y_index} on distance matrix')

		if self.distance_matrix[x_index][y_index] != None:
			return self.distance_matrix[x_index][y_index]

		return self.__get_distance(y_index, x_index)


	def __get_neighbours(self, x_index):
		"""Return neighbours indexs in points by eps for x_index"""
		neighbours = []
		for y_index in range(0, self.count_of_points):
			if y_index == x_index:
				continue

			if self.__get_distance(x_index, y_index) <= self.eps:
				neighbours.append(y_index)
		
		return neighbours
	
	def __get_avg_m(self, x_index, m):
		vector = []
		for y_index in range(0, self.count_of_points):
			if y_index == x_index:
				continue

			vector.append(self.__get_distance(x_index, y_index))

		return np.array(sorted(vector)[:m]).mean()

	def __pick_new_cluster(self, x_index, neighbour_indexs):
		self.count_of_clusters += 1
		self.logger.debug(f"Cluster {self.count_of_clusters} expand...")
		cluster_marker = self.count_of_clusters
		if self.count_of_clusters not in self.clusters:
			self.clusters[cluster_marker] = []

		self.clusters[cluster_marker].append(x_index)
		self.clustered_points[x_index] = cluster_marker

		while neighbour_indexs:
			y_index = neighbour_indexs.pop()

			if y_index not in self.visited_points:
				self.visited_points.add(y_index)
				neighbours = self.__get_neighbours(y_index)
				if len(neighbours) >= self.min_count_of_points:
					filtered_points = []
					for point in neighbours:
						if point not in self.visited_points:
							filtered_points.append(point)
					neighbour_indexs.extend(filtered_points)	
			
			if y_index not in self.clustered_points:
				self.clusters[cluster_marker].append(y_index)
				self.clustered_points[y_index] = cluster_marker
				if y_index in self.clusters[self.noise_cluster_marker]:
					self.clusters[self.noise_cluster_marker].remove(y_index)


	def compute_opt_eps(self, points, m, dist=default_distance, verbose=True):
		self.__init(points,0,m,dist,verbose)
		result = []
		for x_index in range(0, self.count_of_points):
			result.append(self.__get_avg_m(x_index, m))

		return sorted(result)
			




