from sklearn import metrics
from dbscan.dbscan import DBSCAN
from utils import get_logger
import pandas as pd
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN as skDBSCAN
from sklearn.metrics import adjusted_rand_score, homogeneity_score, completeness_score, v_measure_score, adjusted_mutual_info_score, silhouette_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, SpectralClustering

def compute_metrics(x_train, y_train, y_pred_train):
	return {
		"ARI": adjusted_rand_score(y_train, y_pred_train),
		"Completeness": completeness_score(y_train, y_pred_train),
		"Homogeneity": homogeneity_score(y_train, y_pred_train),
		"V-measure": v_measure_score(y_train, y_pred_train),
		"AMI": adjusted_mutual_info_score(y_train, y_pred_train),
		"Silhouette": silhouette_score(x_train, y_pred_train)
	}

def draw_data(X_test, y_test, name):
	tmp = pd.DataFrame(data=X_test)
	tmp['y'] = np.asarray(y_test)
	g = sns.pairplot(tmp, hue='y', markers='*')
	g.savefig(name)

def main():
	print('Iris')
	df = pd.read_csv('datasets/Iris.csv')
	df = df.drop("Id",axis=1)
	data = df.copy()
	goal_field = 'Species'
	name = 'Iris'

	observations = df.drop([goal_field],axis=1).to_numpy()
	classes = np.sort(df[goal_field].unique())
	for index, c in enumerate(classes):
		df.loc[df[goal_field] == c, goal_field] = index
	classes	 = df[goal_field].unique()

	X = observations
	y_true = df[goal_field]
	# draw_data(X, y_true,"Iris.png")

	clusterer = DBSCAN()
	m = 4
	# means = clusterer.compute_opt_eps(X, m, verbose=True)
	# ax = sns.lineplot(x=np.array(range(0,len(means))), y=np.array(means))
	# ax.set(xlabel=f'Порядковый номер точки', ylabel=f'Среднее расстояние до ближайших {m} соседей')
	# plt.show()

	eps = 0.45
	m = 4
	y_pred = clusterer.fit(X, eps, m, verbose=True)

	metrics_on_train = compute_metrics(X, y_true, y_pred)
	print(pd.DataFrame([metrics_on_train]).round(2))
	draw_data(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png")

	# metrics_on_train = clusterization(data, goal_field, name, count_of_clusters=3, draw_plots=True)

	# print(pd.DataFrame([metrics_on_train]).round(2))

	# print('Iris')
	# df = pd.read_csv('datasets/Iris.csv')
	# df = df.drop("Id",axis=1)
	# draw_data(df.drop("Species", axis=1).to_numpy(),df["Species"].to_numpy(),"Iris.png")
	# compute_optim_k(df,'Species','Iris',10, True)

	# print('Wine')
	# df = pd.read_csv('datasets/Wine.csv')
	# df_filtered =  df[['Wine','Color.int','Hue','OD','Proline']]
	# X = df_filtered.drop('Wine', axis=1)
	# y_true = df["Wine"].to_numpy()
	# draw_data(X, y_true,"Wine.png")
	# compute_optim_k(df_filtered,'Wine','Wine',10, True)

	# name = 'blobs-6'
	# n_samples = 500
	# n_components = 6
	# X, y_true = make_blobs(n_samples=n_samples,
    #                    centers=n_components,
    #                    cluster_std=0.60,
    #                    random_state=0)
	# draw_data(X, y_true,"Bloobs-6.png")
	# plt.clf()
	# df = pd.DataFrame(X)
	# df['y'] = y_true

	# clusterer = DBSCAN()
	# m = 4
	# means = clusterer.compute_opt_eps(X, m, verbose=True)
	# a4_dims = (14, 5)
	# fig, ax = plt.subplots(figsize=a4_dims)
	# ax = sns.lineplot(x=np.array(range(0,len(means))), y=np.array(means), ax=ax)
	# ax.set(xlabel=f'Порядковый номер точки', ylabel=f'Среднее расстояние до ближайших {m} соседей')
	# plt.savefig(f"{name}-means-{m}-dbscan-pred.png")
	# plt.clf()

	# eps = 0.45
	# y_pred = clusterer.fit(X, eps, m, verbose=True)
	# draw_data(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png")
	# metrics_on_train = compute_metrics(X, y_true, y_pred)
	# print(pd.DataFrame([metrics_on_train]).round(2))


	# compute_optim_k(df,'y','Blobs-6',10)
	# print('Moons')
	# name = 'Moons'
	# n_samples = 400
	# X, y_true = make_moons(n_samples=n_samples,
    #                    noise=.1,
    #                    random_state=0)
	# df = pd.DataFrame(X)
	# df['y'] = y_true

	# draw_data(X, y_true,"Moons.png")

	# clusterer = DBSCAN()
	# m = 4
	# means = clusterer.compute_opt_eps(X, m, verbose=True)
	# a4_dims = (14, 5)
	# fig, ax = plt.subplots(figsize=a4_dims)
	# ax = sns.lineplot(x=np.array(range(0,len(means))), y=np.array(means), ax=ax)
	# ax.set(xlabel=f'Порядковый номер точки', ylabel=f'Среднее расстояние до ближайших {m} соседей')
	# plt.savefig(f"{name}-means-{m}-dbscan-pred.png")
	# plt.clf()

	# eps = 0.125
	# y_pred = clusterer.fit(X, eps, m, verbose=True)
	# draw_data(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png")
	# metrics_on_train = compute_metrics(X, y_true, y_pred)
	# print(pd.DataFrame([metrics_on_train]).round(2))


	# print('Circles')
	# name = 'Circles'
	# n_samples = 400
	# X, y_true = make_circles(n_samples)
	# draw_data(X,y_true,"Circles.png")
	# df = pd.DataFrame(X)
	# df['y'] = y_true

	# clusterer = DBSCAN()
	# eps = 0.1
	# m = 3
	# y_pred = clusterer.fit(X, eps, m, verbose=True)
	# m = 4
	# means = clusterer.compute_opt_eps(X, m, verbose=True)
	# a4_dims = (14, 5)
	# fig, ax = plt.subplots(figsize=a4_dims)
	# ax = sns.lineplot(x=np.array(range(0,len(means))), y=np.array(means), ax=ax)
	# ax.set(xlabel=f'Порядковый номер точки', ylabel=f'Среднее расстояние до ближайших {m} соседей')
	# plt.savefig(f"{name}-means-{m}-dbscan-pred.png")
	# plt.clf()

	# eps = 0.05
	# y_pred = clusterer.fit(X, eps, m, verbose=True)
	# draw_data(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png")
	# metrics_on_train = compute_metrics(X, y_true, y_pred)
	# print(pd.DataFrame([metrics_on_train]).round(2))


	# draw_data_2(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png", eps=eps)
	# metrics_on_train = compute_metrics(x, y_true, y_pred)
	# print(pd.DataFrame([metrics_on_train]).round(2))
	# draw_data(X,y_pred,f"{name}-{eps}-{m}-dbscan-pred.png")

	# sklearn_clusterer = skDBSCAN(eps=eps,min_samples=m)
	# y_pred = sklearn_clusterer.fit(X)
	# draw_data(X,y_pred.labels_,f"{name}-{eps}-{m}-sk-dbscan-pred.png", eps=eps)

	# print('Circles')
	# n_samples = 4000
	# X, y_true = make_circles(n_samples)
	# draw_data(X,y_true,"Circles.png")
	# df = pd.DataFrame(X)
	# df['y'] = y_true
	# compute_optim_k(df,'y','Circles',10, True)

if __name__ == "__main__":
	main()